package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"syscall"
	"time"

	"github.com/u-root/u-root/pkg/dhclient"
	"github.com/u-root/u-root/pkg/libinit"
	"github.com/vishvananda/netlink"
)

const (
	ifupTimeout = 10 * time.Second
	linkTimeout = 3 * time.Second
	dhcpRetries = 5
)

func dhcp() error {
	interfaces, err := net.Interfaces()
	if err != nil {
		return err
	}
	links := make([]netlink.Link, 0, len(interfaces))

	for _, iface := range interfaces {
		log.Printf("Interface %s, mac %s", iface.Name, iface.HardwareAddr.String())
		if iface.Flags&net.FlagLoopback != 0 {
			continue
		}
		link, err := netlink.LinkByName(iface.Name)
		if err != nil {
			log.Printf("LinkByName failed: %v", err)
			continue
		}
		links = append(links, link)
	}
	if len(links) == 0 {
		return fmt.Errorf("no network interfaces")
	}
	results := dhclient.SendRequests(context.Background(), links, true, true,
		dhclient.Config{
			Timeout:  ifupTimeout,
			Retries:  dhcpRetries,
			LogLevel: dhclient.LogDebug,
		}, linkTimeout)

	for r := range results {
		if r.Err != nil {
			log.Printf("%s: DHCP error: %v", r.Interface.Attrs().Name, r.Err)

			continue
		}

		err = r.Lease.Configure()
		if err != nil {
			log.Printf("%s: configuration error: %v", r.Interface.Attrs().Name, err)
		} else {
			log.Printf("%s: DHCP successful", r.Interface.Attrs().Name)

			return nil
		}
	}
	return fmt.Errorf("no dhcp success")
}

func main() {
	// Power off
	if os.Getpid() == 1 {
		log.Print("Running as init")
		defer func() {
			log.Print("Exiting")
			time.Sleep(2)
			syscall.Reboot(0x4321fedc)
		}()

		libinit.CreateRootfs()
		libinit.NetInit()
		if err := dhcp(); err != nil {
			log.Fatalf("Fail: %v", err)
		}
		time.Sleep(10)
	} else {
		log.Printf("Not running as init")
	}
	// www.sigsum.org, serves https redirect at port 80.
	c, err := net.Dial("tcp", "91.223.231.171:80")
	if err != nil {
		log.Fatalf("connecting failed: %v", err)
	}
	if _, err := io.WriteString(c, "HEAD / HTTP/1.0\r\nHost:www.sigsum.org\r\n\r\n"); err != nil {
		log.Fatalf("write req failed: %v", err)
	}
	if _, err := io.Copy(os.Stdout, c); err != nil {
		log.Fatalf("read response failed: %v", err)
	}
}
