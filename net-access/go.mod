module local

go 1.19

require (
	github.com/u-root/u-root v0.10.0
	github.com/vishvananda/netlink v1.1.1-0.20211118161826-650dca95af54
)

require (
	github.com/insomniacslk/dhcp v0.0.0-20211209223715-7d93572ebe8e // indirect
	github.com/klauspost/compress v1.10.6 // indirect
	github.com/klauspost/pgzip v1.2.4 // indirect
	github.com/mdlayher/ethernet v0.0.0-20190606142754-0394541c37b7 // indirect
	github.com/mdlayher/raw v0.0.0-20191009151244-50f2db8cc065 // indirect
	github.com/u-root/uio v0.0.0-20220204230159-dac05f7d2cb4 // indirect
	github.com/ulikunitz/xz v0.5.8 // indirect
	github.com/vishvananda/netns v0.0.0-20210104183010-2eb08e3e575f // indirect
	golang.org/x/net v0.0.0-20211015210444-4f30a5c0130f // indirect
	golang.org/x/sys v0.0.0-20220610221304-9f5ed59c137d // indirect
)
