LINUX_VERSION = v6.5
LINUX_REPO = $(HOME)/hack/linux
LINUX_IMAGE = linux-tree/arch/x86/boot/bzImage
INIT_PROG = hello

.PRECIOUS: initrd-%

default: bin/hello bin/net-access initrd

linux-tree:
	[ -e $@ ] || git clone $(LINUX_REPO) -b $(LINUX_VERSION) $@

$(LINUX_IMAGE): config linux-tree
	[ -e linux-tree/.config ] || (cd linux-tree && ln -s ../config .config)
	$(MAKE) -C linux-tree

bin/hello: hello.c
	mkdir -p bin
	$(CC) -static -Wall -O -o $@ $< 

bin/net-access: net-access/net-access.go
	mkdir -p bin
	cd net-access && CGO_ENABLED=0 go build -o ../$@ .

# xz and cpio options cargo-culted from the mkinitramfs script.
initrd-%: bin/%
	rm -rf tmp-initramfs
	mkdir tmp-initramfs
	cd tmp-initramfs && ln -s ../$< init
	echo init | (cd tmp-initramfs && cpio -o -H newc -R 0:0 --dereference) | xz -z --check=crc32 >$@

boot-%: initrd-% $(LINUX_IMAGE)
	qemu-system-x86_64 -enable-kvm -nographic -no-reboot \
	  -nic user \
	  -object rng-random,filename=/dev/urandom,id=rng0 -device virtio-rng-pci,rng=rng0 \
	  -kernel $(LINUX_IMAGE) -initrd $< -append "console=tty0 console=ttyS0,115200n8 panic=-1" \
	|tee boot.log | tail -40

check: boot-hello
	grep "^Hello init" boot.log >/dev/null || (echo "Failed" ; false)

clean:
	rm -f bin/

.PHONY: boot check clean default
