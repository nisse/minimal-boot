#include <unistd.h>
#include <sys/reboot.h>

int 
main(int argc, char **argv)
{
  static const char hello[] = "Hello init\n";
  static const char bye[] = "Bye init\n";

  write (STDOUT_FILENO, hello, sizeof(hello) - 1);
  sleep (5);
  write (STDOUT_FILENO, bye, sizeof(bye) - 1);
  sleep (1);
  reboot(RB_POWER_OFF);
}
